# Dynamic Graal Isolates
- `Project progress`: ![xx%](https://progress-bar.dev/xx)


## Substrate VM
- Substrate VM is the component which creates native images and is the core repo we are working on.
- SVM supports AoT compilation of Java apps into standalone executable images (i.e native images).
- Native image includes the necessary components like mem management and thread scheduling from substrate VM: deoptimizer, GC, thread scheduling etc

## System Setup.  
- Clone this repo to a directory in local environment, which we will call `graal-root`. Unless stated otherwise, all `cd` commands assume `graal-root` as the top working directory.
```bash
mkdir graal-root && cd graal-root
git clone git@gitlab.com:Yuhala/graal-isolates.git

```

### Installing Graal VM tools
- Note: for `Ubuntu 20.04`,  you may need to properly configure an `http_proxy` to build graal properly. Our tests with `Ubuntu 18.04` worked without any issues.
- Install the [mx](https://github.com/graalvm/mx) build tool:
```bash
git clone https://github.com/graalvm/mx.git
export PATH=$PWD/mx:$PATH

```
- GraalVM's JIT compiler works with the default JVM as a plugin with the help of the JVM compiler interface (JVMCI), and thus requires a JDK which supports a graal-compatible version of JVMCI. You can find a compatible version in the `graal-isolates` repo: `openjdk-8u252+09-jvmci-20.1-b02-linux-amd64.tar.gz`. Otherwise download one here: https://github.com/graalvm/openjdk8-jvmci-builder/releases
- Copy this jdk file into `graal-root`, unzip the archive  and point `JAVA-HOME` to this jdk: 

```bash
cp graal-isolates/openjdk-8u252+09-jvmci-20.1-b02-linux-amd64.tar.gz .
tar xzf <openjdk_name>.tar.gz
export JAVA_HOME=$PWD/openjdk1.8.0_252-jvmci-20.1-b02

```
- Build substrate VM:
```bash
cd graal-isolates/substratevm
mx clean && mx build

```

### Building an example application
- The test application is in `substratevm/Main.java`

- To build and test the sample application, CD into `graal-isolates` directory and run the script: `graal_isolates_main.sh` script. 

```bash
./graal_isolates_main.sh 

```
- This will produce a binary image called `main` in the `substratevm` directory. 
- Move into the substratevm directory and run the application with: `./main`


## Other resources
- Article on Graal compiler by Chris Seaton: https://chrisseaton.com/truffleruby/jokerconf17/
- Medium article on native images by Christian Wimmer: https://medium.com/graalvm/isolates-and-compressed-references-more-flexible-and-efficient-memory-management-for-graalvm-a044cc50b67e
