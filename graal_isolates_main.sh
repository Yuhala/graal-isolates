#!/bin/bash
#
# Copyright (c) 2020 Peterson Yuhala, IIUN
#

# You must be in the graal-sgx directory as root here: ie make sure PWD = graal-sgx dir
SVM_DIR="$PWD/substratevm"
APP_DIR="$PWD/substratevm/app"
JAVAC="$JAVA_HOME/bin/javac"



# Give this file x permission if does not have. Its present perm will probably be: rw-rw-r-- (664). Change to 764
# chmod 764 /path/to/graal-sdk.jar
GRAAL_HOME="$PWD/sdk/latest_graalvm_home/jre/lib/boot/graal-sdk.jar"
GRAAL_SVM="$PWD/sdk/latest_graalvm_home/jre/lib/svm/builder/svm.jar"
GRAAL_SDK_BIN="$PWD/sdk/mxbuild/src/org.graalvm.nativeimage/bin"

# native image build options
SVM_OPTS="-H:+UseOnlyWritableBootImageHeap"


cd $SVM_DIR

mx build

#compile app classes
#$JAVAC -sourcepath $APP_DIR -cp $GRAAL_HOME:$GRAAL_SVM:$GRAAL_SDK_BIN:$SVM_DIR $APP_DIR/*.java


# echo $APP_DIR
$JAVAC -sourcepath . -cp $GRAAL_HOME:$GRAAL_SVM:$GRAAL_SDK_BIN:$SVM_DIR Main.java
mx native-image $SVM_OPTS Main

